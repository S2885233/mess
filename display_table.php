<?php 
    
    
    $fk_global_assoc;
    
     function generate_row_old ($i) {           //Generic one for two columns
        global $rows;
        $gen =  
        "<tr>
            <td>".$rows[$i][0]."</td>
            <td>".$rows[$i][1]."</td>
        </tr>";
        
        return $gen;
    }     

                
    function generate_many_tables($mysqli, $table_name, $editable) {        // Generate all the tables which have many-to-many relationships with the "$table_name"         --      Maybe have some 'WHERE ID = X' for stuff
        $result_tables = $mysqli->query("SHOW TABLES;");
        $data_tables = $result_tables->fetch_all();

 
        for ($i = 0; $i < count($data_tables); $i++ ) {
            if ($table_name != $data_tables[$i][0] && (strstr($data_tables[$i][0], $table_name) != false)) {     // The values contains the table name but is not the table name     --  Should be the many-to-many tables
            
                // Get only the name of the foreign table
                $foreign_table = str_replace($table_name, "", $data_tables[$i][0]);
                    // Leave in the "_" so I can tell the order
                
                if ($foreign_table == "_") {
                    $foreign_table = $table_name;
                }
            
                generate_table($foreign_table, $editable, null);
            }
        }        
    }

    function generate_many_tables_with_id($mysqli, $table_name, $editable, $id) {        // Only generate table using rows in a relationship with the id
        $result_tables = $mysqli->query("SHOW TABLES;");
        $data_tables = $result_tables->fetch_all();

        for ($i = 0; $i < count($data_tables); $i++ ) {
            if ($table_name != $data_tables[$i][0] && (strstr($data_tables[$i][0], $table_name) != false)) {     // The values contains the table name but is not the table name     --  Should be the many-to-many tables

                // Get only the name of the foreign table
                $foreign_table = str_replace($table_name, "", $data_tables[$i][0]);
                    // Leave in the "_" so I can tell the order
                $clean_foreign_table = str_replace("_", "", $foreign_table);
                
                if ($clean_foreign_table == "") {
                    $clean_foreign_table = $table_name;   
                    $foreign_table = $table_name;   
                    
                }
                $where_string = "";

                if ($clean_foreign_table == $table_name) {
                    
                    $query_string_to = create_query_string($clean_foreign_table, $id, "to_", "AND", "");
                    $query_string_from = create_query_string($clean_foreign_table, $id, "from_", "AND", "");

                    // Show the table of ____ which come before the id                    
                    $result = $mysqli->query("SELECT ".$query_string_from[0]." FROM ".$data_tables[$i][0]." WHERE ".$query_string_to[1].";");
                    $data = $result->fetch_all();
                    make_array_of_ids($data, $foreign_table, $editable, "_preceding");

                    if (!$editable) {
                        // Show the table of ____ which come after the id
                        $result = $mysqli->query("SELECT ".$query_string_to[0]." FROM ".$data_tables[$i][0]." WHERE ".$query_string_from[1].";");
                        $data = $result->fetch_all();
                        make_array_of_ids($data, $foreign_table, false, "_following");                        
                    }


                } else {
                    $query_string = create_query_string($table_name, $id, "", "AND", "");
                    $query_string_foreign = create_query_string($clean_foreign_table, $id, "", "AND", "");

                    $result = $mysqli->query("SELECT ".$query_string_foreign[0]." FROM ".$data_tables[$i][0]." WHERE ".$query_string[1].";");
                    $data = $result->fetch_all();
                    make_array_of_ids($data, $foreign_table, $editable, "");

                }
                
            }
        }        
    }
    
    function make_array_of_ids($data, $foreign_table, $editable, $type) {
        if (count($data) == 0) {
            return;    
        }
        
        $array_of_ids = [];
        for ($j = 0; $j < count($data); $j++) {
            $array_of_ids[$j] = $data[$j];                    
        }

            generate_table($foreign_table, $editable, $array_of_ids);
    }
    
    function generate_table($table_name, $editable, $array_of_ids) {           // Only get rows which have these ids

        $table_name_clean = str_replace("_", "", $table_name);
        $mysqli = connect_to_server();
        
        $composite = full_key($table_name_clean);

        $where_string = "WHERE ";
        
        for ($i = 0; $i < count($array_of_ids); $i++) {
            $query_string = create_query_string($table_name_clean, $array_of_ids[$i], "", "OR", "");
            $where_string = $where_string . $query_string[1]." OR ";
        }
        $where_string = substr($where_string, 0, -4);
        
        if (count($array_of_ids) == 0) {
            $where_string = "";
        }

        $full_query = "SELECT * FROM ".$table_name_clean." ".$where_string.";";
        $result = run_query($mysqli, $full_query);
        $table_data = $result->fetch_all();
        
        $head_query = "SHOW COLUMNS FROM ".$table_name_clean.";";
        $resultHead = run_query($mysqli, $head_query);

        $heading_data = $resultHead->fetch_all();

        global $fk_global_assoc;
        $fk_global_assoc = [];
        $fk_global_assoc = associate_id_to_foreign_value($table_name_clean, $heading_data);

// if array_ids is given and the table is editable it must be an update/edit form
        $index_array = [];
        if ($editable && $where_string != "") {
            $full_query = "SELECT * FROM ".$table_name_clean.";";
            $full_result = run_query($mysqli, $full_query);            
            $full_data = $full_result->fetch_all();
            $index_array = find_row_indexes($full_data, $table_data, $composite);   
            $table_data = $full_data;
        }

        $table_id = htmlentities($table_name_clean)."_".htmlentities($editable);

        echo "<div class='table_container'>". present_word($table_name_clean)."(s)</br>";
            create_search($table_id);
            echo "<div class='table_overflow'>";

                echo "<table id='".$table_id."'>";      // editable is used to differentiate between multiple of the same table (but different edit attri) 
                                                                                        //maybe not the best way but multiple of the same table with same edit attri would be redundant most of the time
                                                                                        // def not best way, in a recursive one the a->b and b->c tables have the same id
                                                                                                                // a->b could be used on its own
                                                                                                // could just have a global id variable which increments

            echo "<tr>";
                if ($editable === true) {
                    echo "<th></th>";
                }
                for ($i = 0; $i < count($heading_data); $i++) {
                    generate_header_row($table_id, $heading_data[$i][0]); 

                }   
                echo "</tr>";
                generate_in_order($table_name_clean, $table_name, $table_data, "id", "desc", $editable, $composite, $index_array);
       
                echo "</table></br>";
            echo "</div>";        
        echo "</div>";        
        
    }
        

    function generate_in_order ($table_name_clean, $table_name, $table_data, $order_by, $direction, $editable, $composite, $index_array) {       // Having it here would require it be done on page load or async, maybe do it in js
        // this could just be move into generate_table, I think
        for ($i = 0; $i < count($table_data); $i++) {

            if (count($index_array[$i]) != 0) {
//            if ($index_array[$i] != null) {   // if the array is only a 0 then it counts as null
              generate_row($table_name_clean, $table_name, $table_data[$i], $editable, $composite, true);
       
            } else {
              generate_row($table_name_clean, $table_name, $table_data[$i], $editable, $composite, false);
                
            }

        }

    }
    
    function generate_header_row ($table_id, $head_cell_data) {           // Make the top row with all the names of the columns,        but the column names have underscores and stuff in
        echo "<th class='header_cell' onclick=\"reorder_table('".$table_id."', '".htmlentities($head_cell_data)."')\">".htmlentities($head_cell_data)."</th>";
    }
    
    function generate_row ($table_name_clean, $table_name, $row_data, $editable, $composite, $checked) {           //Generic one for the skill thing
        $select_string = "";
        
        for ($k = 0; $k < count($row_data); $k++) {
            if ($composite[$k] != "") {
                $select_string = $select_string . $row_data[$k] . "_comp_";    
               
            }
        }
        $select_string = htmlentities($select_string);
        $table_name_clean = htmlentities($table_name_clean);
        $table_name = htmlentities($table_name);
        
        if ($editable === "single-click") {
            if ($checked) {
                echo "<tr class='placeable_row highlighted_row' onclick=\"add_to_equation('".$table_name_clean."', '".$select_string."')\" id='".$table_name_clean."_".$select_string."'>";        // This one is without the "_"
            } else {
                echo "<tr class='placeable_row' onclick=\"add_to_equation('".$table_name_clean."', '".$select_string."')\" id='".$table_name_clean."_".$select_string."'>";        // This one is without the "_"
            }    
            
        } else if ($checked) {
            echo "<tr class='editable_row'>";    
            echo "<td><input type='checkbox' name='".$table_name."[]' value='".$select_string."' checked></td>";          // Keep the "_"
            
        } else if ($editable) {
            echo "<tr class='editable_row'>";    
            echo "<td><input type='checkbox' name='".$table_name."[]' value='".$select_string."'></td>";          // Keep the "_"
            
        } else {
            echo "<tr class='clickable_row' onclick=\"go_to_record('".$table_name_clean."', '".$select_string."')\">";        // This one is without the "_"
                                                                                                        // todo swap above with an array if possible
        }

        for ($i = 0; $i < count($row_data); $i++) {
            generate_cell($row_data[$i], $i);
        }           
        echo "</tr>";
    }     
    
    function generate_cell ($cell_data, $i) {           // A cell
        global $fk_global_assoc;
        if ($fk_global_assoc[$i] != null) {
            echo "<td class='searchable'>".htmlentities($fk_global_assoc[$i][$cell_data])."</td>";
        } else {
            echo "<td class='searchable'>".htmlentities($cell_data)."</td>";
            
        }
       
    }
    
    function create_search($table_id) {
        echo "Search: <input type='text', id='search_".$table_id."' oninput=\"search_table('".$table_id."')\">";
    }
?>

<script type="text/javascript">

    var order = ["id", "desc"];         // todo this doesn't work with multiple tables which could all have their own orders
    
    function reorder_table(table_id, column_name) {     // Re call generate in order

        // Get the order, if the column is the same change the direction
        var direction = "desc";
        if (column_name == order[0]) {
            if (order[1] == "desc") {
                direction = "asc";
            } else {
                direction = "desc";                
            }
        }
        order = [column_name, direction];
        
        
        // Order the thing
        var table_data = document.getElementById(table_id);
        var row_array = table_data.getElementsByTagName('tr');

        var cells = new Array();
        var sorting_array = new Array();

        var head_cells = row_array[0].getElementsByTagName("th");
  
        var column_index;
        
        // Find which column is the one being sorted
        for (var j = 0; j < head_cells.length; j++) {
            if (head_cells[j].innerHTML == column_name) {
                column_index = j;
            }
        }
        
        // Get all the data from the column being sorted and put it in an array with
        for (var i = 1; i < row_array.length; i++) { // -1 because "</table> is stuck" on the end of the thing
            cells = row_array[i].getElementsByTagName("td");

            sorting_array[i-1] = cells[column_index].innerHTML+"_"+i;   // "i" which is the index of the row because you can't order maps in js it seems

    
        }
        
        var sorted = new Array();
        sorted = sorting_array.sort();              // 1, 10, 11, 12 ..., 19, 2, 20     -- Not good

        if (direction == "asc") {
            sorted = reverse_array(sorted);
        } 

        var sorted_ids = new Array();
        for (var i = 0; i < sorted.length; i++) {

           sorted_ids[i] = sorted[i].split("_")[1];
        
        }
        
               
        // Display the thing
        var generated_table = row_array[0].outerHTML;
        for (var i = 0; i < sorted_ids.length; i++) {
              generated_table += row_array[sorted_ids[i]].outerHTML;
        }
        document.getElementById(table_id).innerHTML = generated_table;
    }
    
    function reverse_array(a) {
        var array = new Array();
        var len = a.length - 1;
        for (var i = (len); i >= 0; i--) {
            array[len - i] = a[i];
        }
        return array;
    }   
    
    function go_to_record(table_name, comp) {
        window.location = "show_record.php?table=" + table_name + "&id=" + comp + "&edit=false";
    }
    
    function search_table(table_id) {           // search the table for all the rows which have the search value and makes those without invisible
        var search_value = document.getElementById('search_'+table_id).value;
        var table_data = document.getElementById(table_id);

        var row_array = table_data.getElementsByTagName('tr');
        for (var i = 1; i < row_array.length; i++) {            // row 0 is header row
            var cell_array = row_array[i].getElementsByClassName('searchable');
            var found = false;
            for (var j = 0; j < cell_array.length; j++) {
                if (cell_array[j].innerHTML.indexOf(search_value) > -1 ) {
                    found = true;
                    break;
                }
            }
            if (found) {
                row_array[i].classList.remove("invisible");
            } else {
                row_array[i].classList.add("invisible");                
            }
            
        }
    }
    
</script>
