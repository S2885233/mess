<?php



function unique_generate_equation($equation) {  // draws the equation thing as well
    $table_name = "course";
    $comp_key = full_key($table_name);    
    
    echo "<script type='text/javascript'>";
        $echo_string = "var ".$table_name."_var =  {";
        foreach ($comp_key as $index => $name) {
            $echo_string = $echo_string . "'".$index."':'".$name."',";
        }
        $echo_string = substr($echo_string, 0, -1)."};";
        echo $echo_string;
    echo "</script>";
    

    echo "<div class='equation_creation'>";
        echo "<div class='form_input'>".present_word($table_name)." prerequisite(s): <input class='input_box' id='".$table_name."_equation_box' type='text' name='".$table_name."_equation' value='".$equation."' >";    
            generate_table($table_name, "single-click", null);
        echo "*Optional</div><br>";
    echo "</div>";  
    
    unique_solve_equation($equation, $table_name."_single-click");
}

function unique_solve_equation($equation, $table_id) {         // takes an equation like (2093_01 AND 3999_02) OR (1111_01) and turns that into something, it's for prerequisite course and trying to make that understandable
    
    
}






?>

<script type="text/javascript">
    function add_to_equation (table_name, comp_id) {
        var text_box = document.getElementById(table_name + '_equation_box');
        var table_keys = window[table_name];
        var ids = comp_id.split("_comp_");
        var comp_id_no_comp = comp_id.replace("_comp_", "_");;
        while (comp_id_no_comp.indexOf("_comp_") != -1) {
            var comp_id_no_comp = comp_id_no_comp.replace("_comp_", "_");
        }
        comp_id_no_comp = comp_id_no_comp.substr(0, comp_id_no_comp.length-1);
        var pos = text_box.value.indexOf(comp_id_no_comp);

        if (pos > -1 ) {
            var replace_string = text_box.value.substr(pos, comp_id_no_comp.length);
            text_box.value = text_box.value.replace(replace_string + " ", "");
            document.getElementById(table_name+"_"+comp_id).classList.remove("highlighted_row");            
        } else {
            text_box.value += comp_id_no_comp + " ";
            document.getElementById(table_name+"_"+comp_id).classList.add("highlighted_row");            

        }
    }                

    
</script>