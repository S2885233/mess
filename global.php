<!DOCTYPE html>
<html>
<head>
       <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
</head>

<?php

include 'header.php';
    include 'calc.php';
    
function connect_to_server() {
    return new mysqli("localhost", "s2885233", "", "mydb");                  // These will need to be changed later      -    host, user, password, database
}

function run_query($mysqli, $query) {
    $result = $mysqli->query($query);  // input    -   FROM many-to-many table

    if ($mysqli->error != "") {
        debug_to_screen($query);
        debug_to_screen($mysqli->error);
    } else {
        return $result;
    }
    
}

function escape_string_sql($mysqli, $string, $replace, $with) {
    // I think I remember the given was meant to have trouble with complicated queries      --  but I can't remember so it might not be needed
    if ($replace === "" || $replace == null) {
        $replace = "-?-";
    }

    for ($i = 0; $i < count($with); $i++) {
        //str_replace($replace, $mysqli->real_escape_string($with[$i]), $string, 1);
    }
    
    return $string;
}

function present_word($string) {   // Present a word / phrase in a nice way
    if (strstr($string, " ")) {
        return ucwords($string);
    } else if (strstr($string, "_")) {
        return ucwords(implode(" ", explode("_", $string)));
    } else {
        return ucfirst($string);
    }
}

function debug_to_console( $data ) {        // Debug some object/ array to the internet browser console
    if ( is_array( $data ) ) {
        $output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
    }
    else {
        $output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";
    }

    echo $output;
}

function debug_to_screen( $data ) {        // Debug some object/ array to the screen
    if ( is_array( $data ) ) {
        $output = "</br>Debug Objects: " . implode( ',', $data) . "</br>";
    }
    else {
        $output = "</br>Debug Objects: " . $data . "</br>";
    }

    echo $output;
}


function debug_to_screen_recursive ( $data, $note ) {        // Debug some object/ array to the screen
    if ( is_array( $data ) ) {
        $output = "</br>Debug Objects: </br>" . recursive_array($data, 4) . "</br>";
    }
    else {
        $output = "</br>Debug Objects: " . $data . "</br>";
    }

    echo $output . " " . $note;
}

function recursive_array ($data, $level) {
    $output = "";
    if (is_array($data)) {
        $output = "<span style='padding-left:".$level."em'> --Array: [ </span></br>";
        for ($i = 0; $i < count($data); $i++) {
            $output = $output . recursive_array($data[$i], $level+4) .", </br> ";            
        }        
        $output = $output . "<span style='padding-left:".$level."em'>] length: ". $i. ". </span>";
    } else {
        $output = "<span style='padding-left:".$level."em'>". $data. "</span>";
        
    }
    return $output;
}
//        debug_to_screen($mysqli->error);


?>

<script type="text/javascript">
    function validate_form() {          // not currently needed but would be for form validation
        return true;
        var fields = document.forms["add_record_form"];
        for (var i = 0; i < fields.length; fields++) {
                        
        }

    }
    
</script>