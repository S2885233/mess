


<?php
    

function generate_report ($id, $table_name, $ending_table, $user_id, $workload) {           // generate the report      -   if $workload is 0 it disregards semesters  
    $ordered_array = [];                                                                            // todo all course will need an offered thing
    $mysqli = connect_to_server();
    
    
    $ending_path = find_ending($mysqli, $table_name, $ending_table);
    $ids = [];

    if (is_array($id)) {
        $ids[$table_name] = $id;
        
    } else {
        $ids[$table_name] = [$id];
    
    }
    $i = 1;
    for ($i; $i < count($ending_path[1]); $i++) {      // can skip 0       -   get all the ids connected to the $id

        $recursive_table = $ending_path[2][$i];
        $composite_input = full_key($ending_path[0][$i-1]);        // check for composite key in input table
        $composite_output = full_key($ending_path[0][$i]);        // check for composite key in output table
        
        $column_string = "";
        
        for ($j = 0; $j < count($composite_output); $j++) {
            $column_string = $column_string . $composite_output[$j] . ", ";
        }

        $query_string = create_query_string($ending_path[0][$i-1], $ids[$ending_path[0][$i-1]], $ending_path[1][$i].".", "OR", "");       // create query string for input table with ids from previous loop or $table_name
        $where_string = "(". $query_string[1] . ")";
        
        $table_name = $ending_path[1][$i];

        $full_query = "SELECT ".$column_string." COUNT(".$ending_path[1][$i]. ".". $composite_output[0].") as counted FROM ".$table_name." WHERE ".$where_string." GROUP BY ".$composite_output[0]." ORDER BY counted DESC;";
        $result = run_query($mysqli, $full_query);

        $data = $result->fetch_all();
        
        $ids[$ending_path[0][$i]] = extract_ids($data, $composite_output);

        
       if (!is_array($ids[$ending_path[0][$i-1]])) {    // is not many to many relationship, or at least not being used as one
            $ordered_array = walk_array($ids[$ending_path[0][$i]], $workload);
            
        } else {
            $join_string = "";
            $already_used = [];
            $to_be_ordered_array = [];
            if ($recursive_table != "") {       // there is not a recursive table       -- or there is a recursive table ??
                $join_string = " AND ( ". create_join_string($recursive_table, $ending_path[1][$i], "<>") .") ";
            }  

            $covered = 0;
            while (count($ids[$ending_path[0][$i-1]]) == $covered) {
                $usable = unique_get_usable($ids[$ending_path[0][$i]], $already_used);
                $query_string = create_query_string($ending_path[0][$i], $ids[$ending_path[0][$i]], $ending_path[1][$i].".", "OR", "");       // create query string for input table with ids from previous loop or $table_name
                $table_name = $ending_path[1][$i] . ", " . $recursive_table;
                $full_query = "SELECT ".$column_string." COUNT(".$ending_path[1][$i]. ".". $composite_output[0].") as counted FROM ".$table_name." WHERE ".$where_string.$join_string." GROUP BY ".$composite_output[0]." ORDER BY counted DESC;";
                $result = run_query($mysqli, $full_query);
                $data = $result->fetch_all();

                $id_array = extract_ids($data, $composite_output);
                
                step_array($id_array, $k, $workload);
                array_push($to_be_ordered_array = $id_array[0]);
                
                
                // look through the possible courses and find any that must be used (no other course has the skills)
                        // sounds like one of those things in principles, look into that
             
                // find all the skills that can be done initially (prereq is null)
                        // recursive with prereq being null or already done
                            // any skills not done must then be found
                            
                
                // think about how a person would do it and what info they have
                
            }
            
        }

     
    }
    draw_report($ending_path[0][$i-1], $ids, $ordered_array[$i-1]);
    
}

function walk_array ($id_array, $step_size) {       // go through every step in the array       --  split up an array into arrays the size of $step_size
    $array = [];
    if ($workload == 0) {        // ordering is not important
            
    } else {
        for ($k = 0; $k < count($id_array); $k = $k + $step_size) {
        array_push($array, step_array($id_array, $k, $step_size));    

        }   
    }
    return $array;
}

function step_array ($id_array, $start, $step_size) {       // perform a step       --  add the number of things into it's own array
    $step_array = [];
    for ($j = 0; $j < count($step_size); $j++) {
        $step_array[$j] = $id_array[$start + $j];
    }
    return $step_array;
}

function prerequisites ($data, $start) {            // returns an array     
                    // $data should be a many-to-many table, $start should be a key/ compo key
    
}

function unique_get_usable ($id_array, $previously_used) {        // get all the arrays that can be used      --      this function probably won't be as reusable as others
    // check that the semester is right
    // and maybe the $previously_used thing should be a thing
    return $id_array;
}


function draw_report($table_name, $ids, $ordered_array) {
    include 'display_record.php';
    $mysqli = connect_to_server();
    
    foreach ($ordered_array as $order => $id_array) {
        for ($i = 0; $i < count($id_array); $i++) {
            generate_record($mysqli, $table_name, $id_array[$i]);
            
        }
    }
    debug_to_screen_recursive($ordered_array, "note");
}

        if ($recursive_table == "") {            // there is no recursive table so their is no prerequisites to do a thing

        } else {
        // where the ids are not in the recursive table

                 // check for composite key in input table
            $join_string = " AND ( ". create_join_string($recursive_table, $ending_path[1][$i], "<>") .") ";
        
 //           $query_string_not = create_query_string($ending_path[0][$i], $ids[$ending_path[0][$i-1]], $recursive_table.".to_", "AND", "<>");       // create query string for input table with ids from previous loop or $table_name
   //         $where_string_not = " AND (" . $query_string_not[1] . ") ";
            
            $table_name = $ending_path[1][$i] . ", " . $recursive_table;
    
        // not ordered, maybe I should do a join so I can order by count again, but this would mean two order by / group by queries which might be slower 
                //  and would require a joining function since I only have WHERE and SELECT things at the moment
        
            $data = $result->fetch_all();

        }

/*
        $result = $mysqli->query("SELECT *, COUNT(".$composite_output[0].") as counted FROM ".$ending_path[1][$i]." WHERE ".$query_string[1]." GROUP BY ".$composite_output[0]." ORDER BY counted;");  // input    -   FROM many-to-many table

        
        $data = $result->fetch_all();
        
        for ($j = 0; $j < count($data); $j++) {
            $add = [];
            foreach ($composite_output as $index => $value) {
                array_push($add, $data[$j][$index]);        // output per composite
            }
            $ids[$ending_path[0][$i]][$j] = $add;
        }
        
        // do the algorithm thing here with         --      maybe I should move it to the drawn report and run it through js
        if (is_array($ids[$ending_path[0][$i-1]])) {
            $ordered_array[$ending_path[0][$i]] = perform_algorithm($mysqli, $ids[$ending_path[0][$i]], $ids[$ending_path[0][$i-1]], $workload, $ending_path[0][$i], $ending_path[2][$i]);

        } else {        // Doesn't need to be algorithmed
        
            
        }
*/
function perform_algorithm ($mysqli, $covering, $to_be_covered, $workload, $table_name, $recursive_table) {         // $covering will go over the $to_be_covered as efficiently as possible
    if ($recursive_table == "") {
        // there is no recursive table so their is no prerequisites to do a thing
        if ($workload == 0) {
            return $covering;
        }
        $ordered_array = [];
        for ($i = 0; $i < count($covering); $i = $i + $workload) {
            $step_array = [];
            for ($j = 0; $j < count($workload); $j++) {
                $step_array[$j] = $covering[$i + $j];
            }
            array_push($ordered_array, $step_array);
        }
        return $ordered_array;
    } else {
        // where the ids are not in the recursive table

        $composite = full_key($table_name);        // check for composite key in input table
        
                                                                                                                                       
                                                                                                                                       
                                                                                                                                       
                                                                                            $join_string = create_join_string($table_name, $foreign_table_name);
        
        
        
        
        $query_string = create_query_string($table_name, $covering, "to_", "AND", "<>");       // create query string for input table with ids from previous loop or $table_name
        
        $result = $mysqli->query("SELECT ".$composite." FROM ".$recursive_table." WHERE ".$query_string[1].";");  // input    -   FROM many-to-many table
    
        // not ordered, maybe I should do a join so I can order by count again, but this would mean two order by / group by queries which might be slower 
                //  and would require a joining function since I only have WHERE and SELECT things at the moment
        
        $data = $result->fetch_all();

    }
}





        


function find_ending ($mysqli, $table_name, $ending_table) {     // find how to get from $table_name to the $ending_table
    return [["job","skill", "course"], ["", "skill_job", "skill_course"], ["", "", "course_course"]];
    // todo - this properly

    $path = [];
    $intermediate_step = "";
    
    $index = 0;
    $path[$index++] = $table_name;
    
    if ($table_name == $ending_table) {
        return $path;
    }
    
    $result_tables = $mysqli->query("SHOW TABLES;");
    $data_tables = $result_tables->fetch_all();

    while ($intermediate_step != $ending_table) {
        for ($i = 0; $i < count($data_tables); $i++ ) {
                search_step();
        }
    }
    return $path;
}

function search_step ($haystack, $needle) {         // you look for the $needle in the $haystack
    if ($table_name != $data_tables[$i][0] && (strstr($data_tables[$i][0], $table_name) != false)) {     // The values contains the table name but is not the table name     --  Should be the many-to-many tables
        if (strstr($data_tables[$i][0], $ending_table)) {   // table contains both the $table_name and the $ending_table    --  it's the many-to-many connecting them
            $path[$index++] = $data_tables[$i][0];
            $intermediate_step = $data_tables[$i][0];        
        }          
    }
    
    for ($i = 0; $i < count($haystack); $i++) {
        if ($haystack[$i] == $needle) {
            
        } else if (strstr($needle, $haystack[$i])) {
            
        }
    }
}


function find_courses_for_jobs($job_id) {               // I could probably make it more generic if I knew how to organise the things properly (according to what is wanted)
                                                                    // Might fit in with generate_tables / generate_tables_with_id
    
    $mysqli = connect_to_server();
    
    $result = $mysqli->query("SELECT * FROM ".$table_name." WHERE ".$table_name."_id='".$job_id."';");
    $row_data = $result->fetch_all();

    
    
    return course_ids;      // Maybe make it an object so I can better do overlaps
    
}

function find_relations ($table_name, $foreign_table_name, $id) {
    
    return $id_array;
}

function cover_relations ($table_name, $foreign_table_name, $id_array) {          // Gets an array of ids and finds a way to cover those ids with the ids of another table
    
    return $id_array2;
}






















// I think I may have to sort out the data before I can do much more
        // prereqs dont loop - although I have a work around for that
        // each job/ course doesn't have all the skills

function rrrrrrrrrrrrrrreport($id) {

    $mysqli = connect_to_server();
        // get all skills needed for the job
        $composite_output = full_key("skill");
        $column_string = "";
        for ($j = 0; $j < count($composite_output); $j++) {
            $column_string = $column_string . $composite_output[$j] . ", ";
        }       
        $column_string = substr($column_string, 0, -2);
        $table_name = "skill_job";
        $query_string = create_query_string("job", $id, "", "OR", "");       // create query string for input table with ids from previous loop or $table_name
        $where_string = "(". $query_string[1] . ")";
        $full_query = "SELECT * FROM ".$table_name." WHERE ".$query_string[1].";";
        $result_skills = run_query($mysqli, $full_query);

        $data_skills = $result_skills->fetch_all();     // now have all the skills needed for this job
        $skill_ids = extract_ids($data_skills, $composite_output);

        
        
        // get all the courses which have the skills
        $composite_input = full_key("skill");        // check for composite key in input table
        $composite_output = full_key("course");        // check for composite key in output table

        $query_string = create_query_string("skill", $skill_ids, "", "OR", "");       // create query string for input table with ids from previous loop or $table_name
        $where_string = "(". $query_string[1] . ")";
        $table_name = "skill_course";
        $full_query = "SELECT * FROM ".$table_name." WHERE ".$where_string.";";
        $result_courses = run_query($mysqli, $full_query);
        $data_courses = $result_courses->fetch_all();
        $course_ids = extract_ids($data_courses, ['1'=>'course_code', '2'=>'school_id']);
    
        // if all the skills can be done from courses without prereq it should be done that way
        
        
        // list all course prereqs      -- liable to change depending on how prereqs are shown
        $composite_output = full_key("course");        // check for composite key in output table
        $query_string = create_query_string("skill", $skill_ids, "to_", "OR", "NOT");       // create query string for input table with ids from previous loop or $table_name
        $where_string = "(". $query_string[1] . ")";
        $table_name = "course_course";
        $full_query = "SELECT * FROM ".$table_name.";";
        $result_prereq = run_query($mysqli, $full_query);
        $data_prereq = $result_prereq->fetch_all();
        //$prereqless_course_ids = extract_ids($data_prereqless, $composite_output);
        
        $prereqless = [];
        $prereqs = [];

        for ($i = 0; $i < count($course_ids); $i++) {
            $is_prereq = false;
            for ($j = 0; $j < count($data_prereq); $j++) {
                if ($course_ids[$i][0] == $data_prereq[$j][2] && $course_ids[$i][1] == $data_prereq[$j][3]) {
                    // has prereq
                    $is_prereq = true;
                    break;
                } else {
                    
                }
                // no prereq
            }
            if (!$is_prereq) {
                array_push($prereqless, $course_ids[$i]);
            } else {
                array_push($prereqs, PrereqPaths::makeIf($course_ids[$i], $data_prereq, $data_courses)->getId());
            }

        }

        // get paths
        $skills_done = [];
        $courses_done = [];
        $skills_to_go = $skill_ids;
        for ($i = 0; $i < count($prereqs); $i++) {
            $cheapest = [];
            // if the course hasn't been done   --  choose which one to follow
                        // add skills needed as a param then check for them     -   maybe don't count towards cost

            $cheapest = PrereqPaths::getWithId($prereqs[$i])->findCheapest($courses_done, $skills_to_go, []);
            // redundancy for doing multiple of a skill still a problem
            $skills_done = array_merge($skills_done, $cheapest[0]);
            $courses_done = array_merge($courses_done, $cheapest[1]);      
            
            for ($j = 0 ; $j < count($cheapest[0]); $j++) {             // remove all the skills which have been done
                foreach ($skills_to_go as $index => $value) {
                    if (in_array($value[0], [$cheapest[0][$j]]) ) {
                        unset($skills_to_go[$index]);
                    }
                }
            }
        }
      
        // fill in rest
        for ($i = 0; $i < count($skill_ids); $i++) {
            $done = false;
            for ($j = 0; $j < count($skills_done); $j++) {
                for ($k = 0; $k < count($skills_done[$j]); $k++) {          // because of the array merge the thing is an array of arrays or something
                     if ($skill_ids[$i][0] == $skills_done[$j][$k]) {
                        $done = true;
                        break;
                    }                   
                }
                if ($skill_ids[$i] == $skills_done[$j]) {
                    $done = true;
                    break;
                }                
            }
            
            // this should be done with preference to those with multiple skills
            if (!$done) {       // get a course from prereqless with the skill needed
                for ($k = 0; $k < count($data_courses); $k++) {
                    if ($skill_ids[$i][0] == $data_courses[$k][0]) {
                        for ($l = 0; $l < count($prereqless); $l++) {
                            if ($data_courses[$k][1] == $prereqless[$l][0] && $data_courses[$k][2] == $prereqless[$l][1]) {           // if the course has the skill and is prereqless
                                array_push($courses_done, [$data_courses[$k][1], $data_courses[$k][2]]);      
                                for ($j = 0; $j < count($data_courses); $j++) {     // add all the skills the course has
                                    if ($data_courses[$k][1] == $data_courses[$j][1] && $data_courses[$k][2] == $data_courses[$j][2]) { 
                                        array_push($skills_done, [$data_courses[$j][0]]);
                                    }
                                }
                                break 2;
                            }
                        }
                    }
                }
            }
        }
        debug_to_screen_recursive($skill_ids, "uds");

        debug_to_screen_recursive($skills_done, "done");
        debug_to_screen_recursive($courses_done, "done");
        
}


// todo ordering of paths and stuff to keep prereqs good

// need to construct skills as well as call them back elsewhere



class PrereqPaths {     // recursive, holds a bunch of paths based upon prereqs and how good each path is
    static private $all = [];     // all the paths/objects
    
    private $id = [];       // id of the course, array for composites
    private $prereq = [];       // connected paths      -- should be split into [1, 2], [1, 3]  --   disjunctive normal form    --  (1 and 2) OR (1 and 3)
    private $cheapest_path = [];     // the cheapest of the connected paths
    private $skills = [];       // skills connected to the course
            // may need to be put in a 2d array, for composite key tables
    
    static function getAll() {
        return PrereqPaths::all;
    }
    
    static function getWithId($id) {
        for ($i = 0; $i < count(PrereqPaths::$all); $i++) {
            if ($id[0] == PrereqPaths::$all[$i]->getId()[0] && $id[1] == PrereqPaths::$all[$i]->getId()[1]) {
                return PrereqPaths::$all[$i];
            }
        }
        return false;
    }
    
    static function makeIf($given_id, $prereq_table, $skill_table) {
        if (PrereqPaths::getWithId($given_id) == false) {
            return new PrereqPaths($given_id, $prereq_table, $skill_table);
        } else {
            return PrereqPaths::getWithId($given_id);
        }
    }
    
    function __construct($given_id, $prereq_table, $skill_table) {    // construct it along with all the connections
        // prereq_table may be changed to $equation and be solved accordingly
        $this->id = $given_id;
        
        $given_skills = [];
        for ($i = 0; $i < count($skill_table); $i++) {
            if ($given_id[0] == $skill_table[$i][1] && $given_id[1] == $skill_table[$i][2]) {
                array_push($given_skills, $skill_table[$i][0]);
                
            }
        }
        $this->skills = $given_skills;
        $index = count(PrereqPaths::$all);
        PrereqPaths::$all[$index] = $this;


        // for the table
        for ($j = 0; $j < count($prereq_table); $j++) {
            if ($given_id[0] == $prereq_table[$j][2] && $given_id[1] == $prereq_table[$j][3]) {
                // has prereq
                array_push($this->prereq, [PrereqPaths::makeIf([$prereq_table[$j][0], $prereq_table[$j][1]], $prereq_table, $skill_table)->getId()]);
                                                // removing [    ]  causes max nesting      --      this was for the possible equation thing
                                                        // max nesting because the a lot of the things all have looped prereqs
                //$prereqs, new PrereqPaths([$data_prereq[0], $data_prereq[1]], $data_prereq);
            } else {
                
            }
        }
        PrereqPaths::$all[$index] = $this;
        return $this;
    }
    
    public function formPrereqs($equation) {             // turn the equation into prereqs
        // look through the thing for AND's, &'s and brackets etc
                // not sure on the syntax for the equation yet though
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getPaths() {
        return $this->prereq;
    }
    
    public function setPaths($ids) {
        array_push($this->prereq, $ids);
    }
    
    public function getSkills($skills_to_go) {       // get all the skills not already done
    // not acknowledging skills not needed so it outputs empty array for prerequisite courses that don't have any skills but are still needed
        $array = [];
        for ($i = 0; $i < count($this->skills); $i++) {
            $done = true;
            for ($j = 0; $j < count($skills_to_go); $j++) {
                if ($this->skills[$i] == $skills_to_go[$j][0]) {
                    $done = false;
                    break;
                }
            }
            if (!$done) {
                array_push($array, $this->skills[$i]);
            }
        }
        return [$this->skills, $array];
    }
    
    public function findCheapest($courses_done, $skills_to_go, $prev) {        // return a list of skills and courses which has the highest skill to course ratio
                        // $courses_done are courses already done, as is $skills_cone

        $cost = 1;
        $courses = [$this->getId()];
        $skills_of_best = $this->getSkills($skills_to_go);
        $profit = count($this->getSkills($skills_to_go)[1]);
        // lowest level will just return above
        
        // Maybe these two could be done better
        if ($profit == 0 && count($prev[0]) == 0) {      // all skills have already been done and is not a prereq for something else
            debug_to_screen("no profit");
            return [[], []];
        }

        for ($i = 0; $i < count($courses_done); $i++) {         // course has alread been done
            if ($courses_done[$i] == $this->getId()) {
                debug_to_screen("already done");
                return [[], []];
            }
        }
        
        if (count($this->prereq) == 0) {
            debug_to_screen("no prereq");
            return [$skills_of_best, $courses];
        }
        $prospective_skill = [];
        $prospective_courses = [];
        $cheapest_cost = 1;
        $cheapest_profit = 1;
        $cheapest = [];
        $cheapest_skills = [];
        $cheapest_courses = [];
        // compare how much each path costs and get the cheapest one

        for ($i = 0; $i < count($this->prereq); $i++) {   // a path

            // cycle through each part of the path to find how much it costs
                    // in the course_course table $this->prereq[$i] should only be one long, it would change if it were the equation
                                        // actually $this->prereq should be one long (because there is only one possible path which is do all the things in the table)
                                        // and the other things many long (all the things in the table)
                                                    // I'm not sure if it matters when using only the course_course table

            for ($j = 0; $j < count($this->prereq[$i]); $j++) {   // a part of a path
                $done = false;
                // check if the course has already been done
                for ($k = 0; $k < count($courses_done); $k++) {
                    if ($this->prereq[$i][$j] == $courses_done[$k]) {
                        $done = true;
                        break;
                    }
                }
                if (!$done) {
                    array_push($prev, $this->getId());
                    $fndfgfgtrs = true;
                    for ($to_be_removed = 0; $to_be_removed < count($prev); $to_be_removed++) {
                        if ($prev[$to_be_removed] == PrereqPaths::getWithId($this->prereq[$i][$j])->getId()) {      // stops looping / nest maxing      --  hopefully can be removed with proper data in prereqs
                            $fndfgfgtrs = false;
                            break;
                        }                       
                    }
                    if ($fndfgfgtrs) {
                            $cheapest = PrereqPaths::getWithId($this->prereq[$i][$j])->findCheapest($courses_done, $skills_to_go, $prev);
                            $cheapest_profit += count($cheapest[0][1]);
                            $cheapest_cost += count($cheapest[1]); 
                    } else {
                    debug_to_screen("loop");
                        return [$skills_of_best, $courses];     // if the thing is a loop it will return this

                    }
                    $cheapest_skills = array_merge($cheapest_skills, $cheapest[0][0]);
                    $cheapest_courses = array_merge($cheapest_courses, $cheapest[1]);

                    // add the costs and profits together
                } else {
                    debug_to_screen("no unused prereq");
                   return [$skills_of_best, $courses];
                }
            }

            // do something about $cost == 1 which means its the lowest level
            //  could hold onto all of them and then check else where for if any of the skills/courses are needed by other things
            if ($cost == 1) {       // first thing because the cheapest would never go below 1
                $prospective_skill = $cheapest_skills;
                $prospective_courses = $cheapest_courses;                    
                $profit = $cheapest_profit;
                $cost = $cheapest_cost;                
            }

            if ($profit / $cost > $cheapest_profit / $cheapest_cost) {
                $prospective_skill = $cheapest_skills;
                $prospective_courses = $cheapest_courses;                    
                $profit = $cheapest_profit;
                $cost = $cheapest_cost;                
            }
        }

        $skills_of_best = array_merge($prospective_skill, $skills_of_best);
        $courses = array_merge($prospective_courses, $courses);
        
        // I want it ordered into an array so one row of the array needs to be completed before starting on the next so prerequisites are met
        
        //$skills_of_best = array_push($prospective_skill, $skills_of_best);
        //$courses = array_push($prospective_courses, $courses);
            
        return [$skills_of_best, $courses];
    }
    
}
// count(array, true) is recursive


?>
