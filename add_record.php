<br>

<form id="add_record_form" action="save_record.php" method="post" onsubmit="return validate_form()" >

<?php
    $mysqli = connect_to_server();

    $table_name = htmlspecialchars($_GET["table"]);
    $id = htmlspecialchars($_GET["id"]);
    
    if ($table_name == null) {
        return;
    }
    
    if ($id != null) {      // editing instead of adding
        $query_string = create_query_string($table_name, $id, "", "AND", "");       // create query string for input table with ids from previous loop or $table_name
        $where_string = "(". $query_string[1] . ")";

        $full_query = "SELECT * FROM ".$table_name." WHERE ".$where_string.";";
        $result_id = run_query($mysqli, $full_query);
        $record_data = $result_id->fetch_row();
        
        $composite = full_key($table_name);

        echo "<input type='hidden' value='update' name='update' />";
        
        foreach ($composite as $index => $name) {
            echo "<input type='hidden' value='".$record_data[$index]."' name='update_".$name."' />";

        }
    }
    
    //$table_name = "course";
    echo "<span class='title'>Add New ".present_word($table_name).":</span></br>";
    
    $full_query = "DESCRIBE ".$table_name.";";
    $result = run_query($mysqli, $full_query);
    $data = $result->fetch_all();
  
    echo "<input type='hidden' value='".$table_name."' name='table_name' />";



    $generated_form = "";
    $equation = false;
    for ($i = 0; $i < count($data); $i++) {
                // Keys

                            // Will need to differentiate between them and other foreign keys
                                // But I haven't really used any foreign keys at the moment
                                
                                
                                // And for the many-to-many tables the foreign keys will be in them rather than in the table being used
 
        if (strstr($data[$i][0], "_equation")) {        // equation thing
            $equation = true;
            
        } else if ($data[$i][3] == "MUL") {        // Is a foreign key

             
            $generated_form = $generated_form . generate_foreign_key_form_row($mysqli, $table_name, $data[$i], $record_data[$i]);


        } else if ($data[$i][3] == "PRI") {          // could be a foreign key
            if (count($record_data) == 0) {
               if (is_foreign_key($table_name, $data[$i][0]) != false) {
                    $generated_form = $generated_form . generate_foreign_key_form_row($mysqli, $table_name, $data[$i], $record_data[$i]);
                  
                } else {
                    $generated_form = $generated_form . generate_normal_form_row($data[$i], $record_data[$i]);
                        
                }
            } else {    // primary key in an update instead of an add should not be editable
                $fk = is_foreign_key($table_name, $data[$i][0]);
                if ($fk != false) {
                    $result_fk = run_query($mysqli, "SELECT * FROM " .$fk[0]. " WHERE " .$fk[1] . "=". $record_data[$i]. ";");
                    $data_fk = $result_fk->fetch_row();
                    echo $data[$i][0] . ":   ". $data_fk[1] . "</br>";  // not very reusable
                  
                } else {
                    echo $data[$i][0] . ":   ". $record_data[$i] . "</br>";  
                }
                          
            }
        } else {

            $generated_form = $generated_form . generate_normal_form_row($data[$i], $record_data[$i]);
        }

    }
    

    echo $generated_form."</br>";

    // todo the record thingo
    if ($id != null) {
        generate_many_tables_with_id($mysqli, $table_name, true, $id);       
    } else {
        generate_many_tables($mysqli, $table_name, true);
        
    }
    
    if ($equation) {
        include 'prerequisite.php';
        if (count($record_data) == 0) {
            unique_generate_equation("");
        } else {
            unique_generate_equation($record_data);
        }
    }
    
    // Make input type go through a javascript function to sanitize and check for integrity or whatever before submitting
    // Might not be necessary
    
    
    function generate_foreign_key_form_row ($mysqli, $table_name, $data_row, $record) {                 // this would be for drop down menus of options
            $foreign_mysqli = new mysqli("localhost", "s2885233", "", "information_schema");                  // These will need to be changed later      -    host, user, password, database

            $foreign_key_result = $foreign_mysqli->query("
                        SELECT *
                        FROM
                            KEY_COLUMN_USAGE
                        WHERE
                            TABLE_NAME = '".$table_name."'
                            AND COLUMN_NAME = '".$data_row[0]."'
                            AND CONSTRAINT_NAME != 'PRIMARY';"
                        );
                        
         
            $foreign_key_data = $foreign_key_result->fetch_all();
            // [0][10] is REFERENCE_TABLE_NAME colum;
            $foreign_table_result = $mysqli->query("SELECT * FROM ".$foreign_key_data[0][10].";");
            
            $foreign_table_data = $foreign_table_result->fetch_all();

             // Generate the string it's an option tag for a drop down list filled with the columns of the referenced table
            $drop_down_menu = "<div class='form_input'>".present_word($data_row[0]) . " <select class='input_box' name='".$data_row[0]."'>";
            
            // $drop_down_menu = $drop_down_menu . "<option value='".$foreign_table_data[$j][0]."'>".$foreign_table_data[$j][1]."</option>";
            //   how do people do optional drop down menus
            for ($j = 0; $j < count($foreign_table_data); $j++) {
                $selected = "";
                
                if ($record == $j) {
                    $selected = "selected='selected'";
                }
 
                $drop_down_menu = $drop_down_menu . "<option ".$selected." value='".$foreign_table_data[$j][0]."'>".$foreign_table_data[$j][1]."</option>";
            }
            $drop_down_menu = $drop_down_menu . "</select></div></br>";
            return $drop_down_menu;     
    }
    
    function generate_normal_form_row($data_row, $record) {
                    
            $input_type = "text";       // Input type
            if (strpos($data_row[1], "int") !== false ){
                $input_type = "number";
            } else if (strpos($data_row[1], "password") !== false ) {
                $input_type = "password";       // Probably should be something more than this
            }
        

        
            $ml;
            $max_length = "";                            // Max length allowed
            if (strpos($data_row[1], "(") !== false ) {
                $begin = strpos($data_row[1], "(");
                $end = strpos($data_row[1], ")");
                $ml = substr($data_row[1], $begin + 1, $end - ($begin + 1));  // The number in the type (I think it should always be the character limit)
            }
            
            if ($input_type == "text") {
                $max_length = "maxlength='".$ml."'";            
                
            } else {
                $max = pow(10, $ml)-1;
                $min = 0;
                if (strpos($data_row[1], "unsigned") == -1) {
                    $min = -1 * $max;
                }
                $max_length = "min= '".$min."' max='".$max."'";            
            }
            
            if (intval($ml) > 50) {
                $input_type = "textarea";
            }

    
            $optional = "";
            $required = "required";
            if ($data[$i][2] == "YES") {        // Field is optional
                $optional = "*Optional";    
                $required = "";
            }

            
            // Default Value
            $default_value = "";        
            if ($record == "") {
                $default_value = $data[$i][4];
            } else {
                $default_value = $record;
            }
            $default = "value='".$default_value."'";          

            
            if ($input_type == "textarea") {
                return "<div class='form_input'>".$data_row[0]."</br><textarea name='".$data_row[0]."' rows='10' cols='80'>".$default_value."</textarea> ".$optional."</div></br>";
            }
            
            return "<div class='form_input'>".$data_row[0].": <input class='input_box' type='".$input_type."' name='".$data_row[0]."' ".$default." ".$max_length." ".$required."> ".$optional."</div><br>";    
    }







?>
    </br>
    <div id="submit_div"><input type="submit"></div>
              
</form>

<br>

<script>
    // Adjust the id submit div to first go through a sanitizing and stuff function
        // Only plays if the user has javascript
        // But still needs data from describe
        // Might be the limit for how far I can go without looking at specific tables
</script>

