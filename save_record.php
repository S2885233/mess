<?php
    include 'global.php';

    $mysqli = connect_to_server();
    $table_name = $_POST['table_name'];
        
    $post_arrays = [];
    $generated_query_where_start = " WHERE ";       
    
    if ($_POST['update'] != null) {
        $generated_query = "UPDATE ".$table_name." SET ";

        foreach($_POST as $column => $value) {
            if ($column == "update") {
                
            } else if ($column == "table_name") {
                
            } else if ($value == "") {     // No value was given
                
            } else if (is_array($value)) {      // If it's an array it must be a something which allows multiple values (one-to-many or many-to-many) and is stored in another table
                // Key (or column?) is table name
                // Values is any array of ids
                $post_arrays[$column] =  uncomposite($value);
                
            } else if (strstr($column, "update_")) {
                $column = str_replace("update_", "", $column);
                $generated_query_where_start = $generated_query_where_start . $column ."='". $mysqli->real_escape_string($value) ."' AND ";    
                
            } else {
                
                
                $generated_query = $generated_query . $column ."='". $mysqli->real_escape_string($value) ."',";       
    
            }
        }
        
        $generated_query = substr($generated_query, 0, -1);    // Remove final comma
        $generated_query_where_start = substr($generated_query_where_start, 0, -5);    // Remove final comma
    
        $generated_query = $generated_query . $generated_query_where_start . ";";              
    } else {
        $generated_query = "INSERT INTO ".$table_name." (";
        $generated_query_values = " VALUES (";
        
        foreach($_POST as $column => $value) {
            if ($column == "table_name") {
                
            } else if ($value == "") {     // No value was given
                
            } else if (is_array($value)) {      // If it's an array it must be a something which allows multiple values (one-to-many or many-to-many) and is stored in another table
                // Key (or column?) is table name
                // Values is any array of ids
                $post_arrays[$column] =  uncomposite($value);
            } else {
                $generated_query = $generated_query . $column . ",";       
                $generated_query_values = $generated_query_values . " '" . $mysqli->real_escape_string($value) . "',";       
    
            }
        }
        
        $generated_query = substr($generated_query, 0, -1);    // Remove final comma
        $generated_query_values = substr($generated_query_values, 0, -1);    // Remove final comma
    
        $generated_query = $generated_query . ")" . $generated_query_values . ");";        
    } 


    // if null
        //        $generated_query = "INSERT INTO ".$table_name.";";


    //$clean_generated_query  = ($generated_query);
    

    $result = run_query($mysqli, $generated_query);
    $id = $mysqli->insert_id;
    $string = $table_name."_id";
    
    if ($id == 0) { 
        $id = $_POST[$string];
    }
    $_POST[$string] = $id;
    
    // Gets the columns and their values for the tables which make up the many-to-many relationship
    $composite_columns = full_key($table_name);
    $composite_value = [];
    for ($k = 0; $k < count($composite_columns); $k++) {
        if ($_POST[$composite_columns[$k]] != null) {
            array_push($composite_value, $mysqli->real_escape_string($_POST[$composite_columns[$k]]));
                
        } else {
            array_push($composite_value, $mysqli->real_escape_string($_POST["update_".$composite_columns[$k]]));
           
        }
    }
        
    $string_array_start = column_value_string($table_name, $mysqli, $composite_value);      // Columns (in [0]) and values (in [1]) from the foreign table
  
        // Start insert rows into the many-to-many tables related to the current table, if applicable
        foreach ($post_arrays as $foreign_table_name => $value_array) {           
 
            // Getting the name of the table, and the order of the columns
                    // The expected value coming through each index of "$foreign_tables" are the name of a table preceded or followed by an underscore, or just an underscore
            $many_to_many_table_name_array = explode("_", $foreign_table_name);  
            $many_to_many_table_values_array = [];
            $recursive = false;
 
            if (($many_to_many_table_name_array[0] == "" && $many_to_many_table_name_array[1] == "") || count($many_to_many_table_name_array) == 1) {     // Table has a recursive relationship
                $many_to_many_table_name_array[0] = $table_name;
                $many_to_many_table_name_array[1] = $table_name;
                $recursive = true;    
            }
            
            if ($many_to_many_table_name_array[0] == "") {
                $many_to_many_table_name_array[0] = $table_name;
            } 
            if ($many_to_many_table_name_array[1] == "") {
                $many_to_many_table_name_array[1] = $table_name;
            } 

            $many_to_many_table_name = implode("_", $many_to_many_table_name_array);

            if ($_POST['update'] != null) {    
                $generated_query_where = $generated_query_where_start;

                if ($recursive) {
                    // do some check to make sure a prerequisite doesn't loop or something, if it shouldn't loop
                    $generated_query_where = " WHERE ";
                    for ($j = 0; $j < count($composite_value); $j++) {
                        $generated_query_where = $generated_query_where . "to_". $composite_columns[$j] ."='". $composite_value[$j] ."' AND ";    
                        
                    }
                    $generated_query_where = substr($generated_query_where, 0, -5);    // Remove final comma
                    
                }
                $generated_query_delete = "DELETE FROM ".$many_to_many_table_name. $generated_query_where .";";
                $result_delete = run_query($mysqli, $generated_query_delete);
            }

            for ($j = 0; $j < count($value_array); $j++) {
                $string_array = $string_array_start;
                // If the foreign table has a composite key it should have been encoded as such - see functions uncomposite and places in display_table.php
                $string_array_second =  column_value_string(str_replace("_", "", $foreign_table_name), $mysqli, $value_array[$j]);      // Columns (in [0]) and values (in [1]) from the foreign table
  
                if ($recursive) {        // I think this one triggers for recursive      -- doesn't work atm anyway
                    for ($l = 0; $l < count($string_array[0]) + count($string_array_second); $l++) {
                        if ($l < count($string_array)) {
                            $string_array[0][$l] = "to_" . $string_array[0][$l];
                        } else {
                            $string_array_second[0][$l-count($string_array)] = "from_" . $string_array_second[0][$l-count($string_array)];
                            
                        }
                    }
                }
                
                $full_column = implode(",", $string_array[0]) . "," . implode(",", $string_array_second[0]);
                $full_value = implode(",", $string_array[1]) . "," . implode(",", $string_array_second[1]);
                $generated_query_foreign = "INSERT INTO ".$many_to_many_table_name." (".$full_column.") VALUES (".$full_value.");";

                $result = run_query($mysqli, $generated_query_foreign);

                
            }
            
        }

function column_value_string($table_name, $mysqli, $value) {            // This function is here in case a table has a composite key
    // Check for a composite key
    
    $generated_composite_column = [];
    $generated_composite_value = [];
    
    $result = $mysqli->query("DESCRIBE ".$table_name.";");
    $data = $result->fetch_all();
    for ($k = 0; $k < count($data); $k++) {             // start at 1 to stip the value which will be in $foreign_table_name_array[0]
        if ($data[$k][3] == "PRI") {
            array_push($generated_composite_column, $data[$k][0]);
            array_push($generated_composite_value, "'".$value[$k]."'");
        }        
    }
    return [$generated_composite_column, $generated_composite_value];
} 

//  do somethin  for the fk

?>