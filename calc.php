<?php


function is_foreign_key($table_name, $column_name) {
    $foreign_mysqli = new mysqli("localhost", "s2885233", "", "information_schema");                  // These will need to be changed later      -    host, user, password, database
    $foreign_key_result = $foreign_mysqli->query("
                SELECT *
                FROM
                    KEY_COLUMN_USAGE
                WHERE
                    TABLE_NAME = '".$table_name."'
                    AND COLUMN_NAME = '".$column_name."'
                    AND CONSTRAINT_NAME != 'PRIMARY';"
                );
                
    if ($foreign_key_result->num_rows != 0) {
        $fetched = $foreign_key_result->fetch_row();
        return [$fetched[10] ,$fetched[11]];      // return the table name and column name of the foreign key's table and column
                                                                                                            // (as in the one connected to the passed in values)
    } else {
        return false;

    }
}






function full_key($table_name) {       // checks if the table has a composite primary key
        $mysqli = connect_to_server();

        $result = $mysqli->query("DESCRIBE ".$table_name.";");
        $data = $result->fetch_all();        
        $array = [];
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i][3] == "PRI") {
                $array[$i] = $data[$i][0];
            }
        }
        return $array;      // assoc array of the index of the column mapped to the name of the column
        
}

function find_row_indexes($full_data, $partial_data, $composite) {      // I dont think it really matters the order of full and partial
    $array = [];
 
   for ($i = 0; $i < count($full_data); $i++) {
        for ($j = 0; $j < count($partial_data); $j++) {
            if (key_equals($full_data[$i], $partial_data[$j], $composite)) {
                  $array[$i] = $j;
                  break;
            }
            
        }
    }
    return $array;
}

function key_equals ($a, $b, $key) {        // check if two rows are the same, as in they have the same primary/composite key
    foreach($key as $index => $value) {
        if ($a[$index] == $b[$index]) {
        } else {
            return false;        
        }
        
    }
    return true;
}
    


function extract_ids ($data, $keys) {       // extract primary/ composite keys from a set of rows
    $id_array = [];
    for ($j = 0; $j < count($data); $j++) {
        $add = [];
        foreach ($keys as $index => $value) {
            array_push($add, $data[$j][$index]);        // output per composite                 -- order is wrong ~~~ or it is taking skill_id rather than school_id
        }
        $id_array[$j] = $add;
    }
    return $id_array;
}

function assoc_array_id_to_row ($table_name) {       // $id can be array of composite keys        -- return associative array of id's to the rest of the row (mainly used for 2 column tables)
    $mysqli = connect_to_server();
    $array = [];
    //has_composite_key($table_name);
    //if (count() > 1) {        // composite
        // todo
    //} else {
        $result = $mysqli->query("SELECT * FROM ".$table_name.";");   
        
        $data = $result->fetch_all();        
        
        for ($i = 0; $i < count($data); $i++) {
            if (count($data[$i]) > 2) {
                for ($j = 1; $j < count($data[$i]); $j++){
                    $array[$data[$i][0]][$j] = $data[$i][$j];
                }               
            } else {
                $array[$data[$i][0]] = $data[$i][1];
                
            }

        }
        return $array;        
//    }
    
}

function associate_id_to_foreign_value($table_name, $heading_data) {
    $array = [];
    for ($i = 0; $i < count($heading_data); $i++) {                             // make it so it outputs a word instead of an id if applicable (see mastery table, school table)
        $fk = is_foreign_key($table_name, $heading_data[$i][0]);
            
        if ($fk != false) {
            $array[$i] = assoc_array_id_to_row($fk[0]);      // todo - connect the output of this to the column name but only when displaying to user not for form value
        }
    }
    return $array;
}

function uncomposite ($string) {        // maybe sql escape it, I think the problem is more them choosing where to put the data (column, table) rather than the data itself which can be solved elsewhere
    $array = [];
    if (is_array($string)) {
        for ($i = 0; $i < count($string); $i++) {
            $array[$i] = explode("_comp_", $string[$i]);
            array_pop($array[$i]);
        }
    } else {
        $array = explode("_comp_", $string);
        array_pop($array);
    }
    return $array;

}


function create_query_string($table_name, $id, $append, $junction, $equals) {        // $append can be left blank, $junction should be "AND" or "OR"

    $mysqli = connect_to_server();
    $table_name = $mysqli->real_escape_string($table_name);
    $append = $mysqli->real_escape_string($append);
    $junction = $mysqli->real_escape_string($junction);
    $equals = $mysqli->real_escape_string($equals);

    if ($junction != "OR" && $junction != "or" && $junction != "AND" && $junction != "and") {
        $junction = "AND";
    }

    $equals = parse_equals($equals);

    $columns = full_key($table_name); 
    $column_string = $append . implode(", ".$append, $columns);        

    $where_string = create_where_string($columns, $id, "", $append, $junction, $equals, $mysqli);


    return [$column_string, $where_string];        
}

function create_where_string ($columns, $id, $previous_string, $append, $junction, $equals, $mysqli) {
    if (is_array($id)) {
        $string = $previous_string;
        for ($i = 0; $i < count($id); $i++) {
             $string = $string . create_where_string($columns, $id[$i], $previous_string, $append, $junction, $equals, $mysqli) . $junction . " ";
           
        }
        return substr($string, 0, -(strlen($junction)+1) );
        
    } else if (strstr($id, "_comp_") != false) {
        $values = uncomposite($id);
        $where_string = "";

        for ($j = 0; $j < count($columns); $j++) {
            $where_string = $where_string . $append . $columns[$j] . $equals . "'". $mysqli->real_escape_string($values[$j]) ."' ". $junction . " ";
            // example output       $where_string .  to_  skill_id  =  '  19  '  AND   
        }
        return substr($where_string, 0, -(strlen($junction)+1) );

    } else {
        return $append . $columns[0] . $equals. "'".$id."' ";

    }
}


function create_join_string ($table_name, $table_name_foreign, $equals) {        // one of these could be a many to many or whatever

    $mysqli = connect_to_server();
    $table_name = $mysqli->real_escape_string($table_name);
    $table_name_foreign = $mysqli->real_escape_string($table_name_foreign);
    $equals = $mysqli->real_escape_string($equals);
    

    $where_string = "";
    $columns;

    $equals = parse_equals($equals);
    $append = "";

    $columns = full_key($table_name); 
    $columns_foreign = full_key($table_name_foreign); 
        
    if (explode("_", $table_name)[0] == explode("_", $table_name)[1]) {   // table_name is a recursive table
        $append = "to_";

    } else if (explode("_", $table_name_foreign)[0] == explode("_", $table_name_foreign)[1]) {      // foreign_table_name is a recursive table
        $append = "from_";

    }
        
    for ($i = 0; $i < count($columns); $i++) {
        for ($j = 0; $j < count($columns_foreign); $j++) {
            if ($append . $columns[$i] == $columns_foreign[$j] || $append . $columns_foreign[$j] == $columns[$i]) {
                $where_string = $where_string . $table_name . ".". $columns[$i] . $equals . $table_name_foreign . ".". $columns_foreign[$j] . " AND ";
            }            
        }
    
    }
    return substr($where_string, 0, -5 );
    
}



function parse_equals ($equals) {
    if ($equals != "NOT"  && $equals != "not" && $equals != "<>" && $equals != "!=") {
        return "=";
    } else {
        return "<>";
    }
}

?>    